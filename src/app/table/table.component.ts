import { Component, Input, OnInit } from '@angular/core';
import { Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {


// child to parent

  @Output() newItemEvent = new EventEmitter<string>();

  @Input() tableData: any;

  constructor() { }

  ngOnInit(): void { }

}
