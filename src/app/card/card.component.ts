import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {
  

  // Input items

  @Input() item: any;

  // To view Output items

  @Output() outputEvent = new EventEmitter();

  constructor() { }






  // Delete the card

  del(item: any){
    this.outputEvent.emit(item);
    console.log('del',this.item);
    // console.log('ARRAY',this.itemArray)
    // this.itemArray.splice(this.itemArray.indexOf(item), 1);
  }





  

// Life cycle hooks

  ngOnChanges(){
    console.log(console.log("OnChanges : ",this.item));
    debugger
  }
  ngOnInit(): void {
    console.log(console.log("OnInit : ",this.item));
    debugger
  }
  ngDoCheck(){
    console.log(console.log("DoCheck : ",this.item));
    debugger
  }
  ngAfterContentInit(){
    console.log(console.log("AfterViewChecked",this.item));
    debugger
  }
  ngAfterContentChecked(){
    console.log(console.log("AfterContentChecked : ",this.item));
    debugger
  }
  ngAfterViewInit(){
    console.log(console.log("AfterViewInit : ",this.item));
    debugger
  }
  ngAfterViewChecked(){
    console.log(console.log("AfterViewChecked : ",this.item));
    debugger
  }
  ngOnDestroy(){
    console.log(console.log("OnDestroy : ",this.item));
  }
}
