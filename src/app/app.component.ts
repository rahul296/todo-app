import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private toastr: ToastrService, private newItemEvent: EventEmitter<string>) {}
  form = new FormGroup({
    name: new FormControl(''),
    });


// parent to child components

  nameArray: Array<any> = []
    
    childData = [
      {No: 1, Name: 'audi', Brand: 'audiCar'},
      {No: 2, Name: 'benz', Brand: 'benzCar'},
      {No: 3, Name: 'bmw', Brand: 'bmwCar'},
    ]

  addTodo(){
    console.log(this.form.value.name);
    if (this.form.value.name?.length && this.form.value.name.length<15){
      this.nameArray.push(this.form.value.name);
    }else {
      this.toastr.error('Please give less than 15 characters');
    }
  }



  // child to parent



  items = [''];

  addItem(newItem: string) {
    console.log(newItem);
    this.items.push(newItem);
  }





  // addNewItem(value: string) {

  //   this.newItemEvent.emit(value);
  // }





  deleteValue(value: string) {
    this.nameArray.splice(this.nameArray.indexOf(value), 1);
  }


}
